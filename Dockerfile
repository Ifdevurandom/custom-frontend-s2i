FROM quay.io/bitnami/nginx
USER 0
EXPOSE 8080
ENV firstPull "successful"
COPY frontend-nginx.conf /opt/bitnami/nginx/conf/nginx.conf
USER 1001
ENTRYPOINT ["/usr/sbin/nginx"]
CMD ["-D","FOREGROUND"]
